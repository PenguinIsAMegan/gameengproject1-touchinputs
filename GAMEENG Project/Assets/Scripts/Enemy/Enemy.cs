using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy/Base")] 
public class Enemy : ScriptableObject
{
    public int health;
    public int speed;
    public int reward;
    public new string name;
    public GameObject prefab;
    public GameObject owner;
}
