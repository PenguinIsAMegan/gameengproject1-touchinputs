using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Unit
{
    public Enemy enemyData;

    // Start is called before the first frame update
    public override void Start()
    {
        Death += Die;

        this.init(enemyData.health, this);

        enemyData.owner = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void BulletHit(int bulletDamage)
    {
        GetHit(bulletDamage);
    }

    public override void Die()
    {
        PlayerController playerController = FindObjectOfType<PlayerController>();
        playerController.addScore(enemyData.reward);
        Destroy(gameObject);
    }
}
