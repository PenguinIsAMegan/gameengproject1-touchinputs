using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionDetector : MonoBehaviour
{
    private EnemyController enemyController;

    private void Start()
    {
        if (this.GetComponent<Health>())
        {
            this.enemyController = this.GetComponent<EnemyController>();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Bullet")
        {
            enemyController.BulletHit(1);//For Bullet, it runs GetHit but can be overrided by armor enemy
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "SuperSlam")
        {
            enemyController.Die();//Instant Kill
        }
    }
}
