using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompTrigger : MonoBehaviour
{
    public GameObject Enemy;
    private EnemyController enemyController;

    private void Start()
    {
        enemyController = Enemy.GetComponent<EnemyController>();
    }
    //Stomping an enemy is an instant kill
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            enemyController.Die();
        }
    }
}
