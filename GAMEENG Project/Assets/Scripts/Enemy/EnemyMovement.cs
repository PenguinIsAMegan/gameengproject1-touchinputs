using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    public float Speed;
    //public int GoldReward = 5; move somwhere else?

    protected Rigidbody2D rb2d;
    protected bool isFacingRight = true;
    protected Vector2 movementVector;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        movementVector = new Vector2(Speed, rb2d.velocity.y);
        Speed = GetComponent<EnemyController>().enemyData.speed;
    }

    void FixedUpdate()
    {
        Move();
    }

    public virtual void Move()
    {

    }
}
