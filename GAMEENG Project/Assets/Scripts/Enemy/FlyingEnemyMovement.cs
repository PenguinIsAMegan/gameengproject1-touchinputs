using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyMovement : EnemyMovement
{
    protected Transform target;
    public float DistanceToPlayer = 20f;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    public override void Move()
    {
        FlyingMovement();
    }

    void FlyingMovement()
    {
        if (target != null)
        {
            rb2d.velocity = new Vector2(0, 0);
            if (Vector3.Distance(target.position, transform.position) <= DistanceToPlayer)
                rb2d.position = Vector2.MoveTowards(transform.position, target.position, Speed * Time.deltaTime);
        }
    }
}
