using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

//Notes:
// First block set should be spawned -8 units relative to the grid's (0,0)
// Spacing between Block sets should be 18 units relative to their center (4 units apart)

public class LevelGeneration : MonoBehaviour
{
    public List<SpawnableChunk> SpawnableBlocks;
    public SpawnableChunk EndLevelBlock;
    public Transform SpawnLocation;

    private SpawnableChunk lastSpawnedBlock;
    public int NumberOfBlockToSpawn = 0;
    private int lastRandNum;

    // Start is called before the first frame update
    void Start()
    {
        GenerateLevel();
    }

    void GenerateLevel()
    {
        for (int i = 0; i < NumberOfBlockToSpawn; i++)
        {
            InstantiateNewBlock();
        }
        InstantiateEndBlock();
    }

    void InstantiateNewBlock()
    {
        int randNum = Random.Range(0, SpawnableBlocks.Count);

        while (randNum == lastRandNum)
        {
            randNum = Random.Range(0, SpawnableBlocks.Count);
        }

        SpawnableChunk nextBlockToSpawn = SpawnableBlocks[randNum];
        lastRandNum = randNum;
        lastSpawnedBlock = Instantiate(nextBlockToSpawn, SpawnLocation.position, transform.rotation);
        lastSpawnedBlock.transform.parent = gameObject.transform;
        SpawnLocation.position += nextBlockToSpawn.SpawnLocation.position;
    }

    void InstantiateEndBlock()
    {
        SpawnableChunk nextBlockToSpawn = EndLevelBlock;
        lastSpawnedBlock = Instantiate(nextBlockToSpawn, SpawnLocation.position, transform.rotation);
        lastSpawnedBlock.transform.parent = gameObject.transform;
        SpawnLocation.position += nextBlockToSpawn.SpawnLocation.position;
    }
}
