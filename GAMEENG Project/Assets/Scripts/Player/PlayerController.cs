using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreModified : UnityEvent<int> { }
public class PlayerController : Unit
{
    public ScoreModified EvtScoreModified = new ScoreModified();
    int score;
    // Start is called before the first frame update
    public override void Start()
    {
        Death += Die;

        this.init(3, this);
        score = 0;
        EvtScoreModified.Invoke(score);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void addScore(int points)
    {
        score += points;
        EvtScoreModified.Invoke(score);
    }

    public override void Die()
    {
        Debug.Log("ded");
        UIManager.Instance.ShowGameOverScreen();
    }
}
