using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwipes : MonoBehaviour
{
    public Vector2 SwipeDelta { get; private set; }
    public bool isDragging = false;
    public bool SwipeUp { get; private set; }
    public bool SwipeDown { get; private set; }
    public bool SwipeRight { get; private set; }
    public bool SwipeLeft { get; private set; }
    public bool Tap { get; private set; }

    private Vector2[] StartTouch = new Vector2[10];
    private Vector2[] EndTouch = new Vector2[10];

    // Update is called once per frame
    void Update()
    {
        Tap = SwipeUp = SwipeDown = SwipeRight = SwipeLeft = false;

        if (Input.touches.Length > 0)
        {
            int i = 0;
            while (i < Input.touchCount)
            {
                Touch touch = Input.GetTouch(i);

                if (touch.phase == TouchPhase.Began)
                {
                    StartTouch[i] = touch.position;
                    EndTouch[i] = touch.position;
                }

                else if (touch.phase == TouchPhase.Ended)
                {
                    isDragging = false;
                    EndTouch[i] = touch.position;

                    if (StartTouch[i] == EndTouch[i])
                        Tap = true;


                    CalculateSwipeDirection(i);

                }

                i++;
            }
        }
    }

    private void CalculateSwipeDirection(int index)
    {
        SwipeDelta = Vector2.zero;
        Touch touch = Input.GetTouch(index);

        SwipeDelta = touch.position - StartTouch[index];

        if (SwipeDelta.magnitude > 150)
        {
            float x = SwipeDelta.x;
            float y = SwipeDelta.y;

            if (Mathf.Abs(x) > Mathf.Abs(y)) // horizontal move
            {
                if (x > 0)
                    SwipeRight = true;
                
                else
                    SwipeLeft = true;
            }
            
            else // vertical move
            {
                if (y > 0)
                    SwipeUp = true;
                
                else
                    SwipeDown = true;
            }      
        }
    }
}
