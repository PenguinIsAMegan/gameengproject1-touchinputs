using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    [SerializeField] private LayerMask platformLayerMask;
    [SerializeField] public SpriteRenderer playerSprite;

    public WeaponManager playerWM;
    public SkillManager playerSM;
    public PlayerSwipes swipe;

    public Rigidbody2D rb2d;
    private BoxCollider2D boxCollider2D;
    private int movementDirection = 0;
    private bool isMoving;
    private bool isButtonPressed;
    
    public bool isSuperSlamActive = false;
    public float GunRecoil = 0.25f;
    public float StompKnockback = 0.5f;
    

    [System.Serializable]
    public class PlayerMotionStats
    {
        public float Speed = 10f;
        public float JumpVelocity = 15f;
    }

    public PlayerMotionStats playerMovement = new PlayerMotionStats();

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.drag = 2;

        boxCollider2D = GetComponent<BoxCollider2D>();
    }


    // Update is called once per frame
    void Update()
    {
        TouchControls();

        if (checkIfGrounded())
            ReloadGun();

        if (isSuperSlamActive)
        {
            if (checkIfGrounded())
            {
                isSuperSlamActive = false; 
                playerSM.DisableSuperSlam();
            }
        }
    }

    void LateUpdate()
    {
        movePlayer();
    }

    // Touch controls
    void TouchControls()
    {
        if (swipe.SwipeUp || swipe.SwipeDown)
            PerformSkill();

        else if (swipe.SwipeRight || swipe.SwipeLeft)
            SwitchGuns();
    }

    public void setMoveDirection(int direction)
    {
        isMoving = true;
        isButtonPressed = true;
        movementDirection = direction;

        if (direction < 0) //left
            playerSprite.flipX = true;
        else if (direction > 0) //right
            playerSprite.flipX = false;
    }

    public void resetMoveDireciton()
    {
        isMoving = false;
        isButtonPressed = false;
        movementDirection = 0;
    }

    // swipe movement version
    private void movePlayer() 
    {
        Vector2 movementVector = new Vector2(movementDirection * playerMovement.Speed, rb2d.velocity.y);
        rb2d.velocity = movementVector;
    }

    private void PerformSkill()
    {
        if (swipe.SwipeDown)
            playerSM.PerformSkill("down");

        else if (swipe.SwipeUp)
            playerSM.PerformSkill("up");
    }

    private void SwitchGuns()
    {
        if (swipe.SwipeRight)
            playerWM.SwitchGun("right");

        else
            playerWM.SwitchGun("left");
    }
    private void jump()
    {
        rb2d.velocity = Vector2.up * playerMovement.JumpVelocity;
    }

    private void shoot()
    {
        ApplyGunRecoil();
        playerWM.PullWeaponTrigger();
    }

    public bool checkIfGrounded()
    {
        RaycastHit2D raycastHit2d = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, .5f, platformLayerMask);

        return raycastHit2d.collider != null;
    }

    public void TriggerJumpShoot()
    {
        if (!checkIfGrounded())
            shoot();

        else if (checkIfGrounded())
            jump();
    }

    public void ApplyGunRecoil() 
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, GunRecoil);
    }

    public void ApplyStompKnockback()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, StompKnockback);
    }

    public void ReloadGun()
    {
        playerWM.ReloadGun();
    }
}
