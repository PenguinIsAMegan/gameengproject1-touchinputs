using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    public GameObject skillPoint;
    public GameObject superSlamCollider;
    public Transform colliderPoint;
    public Health playerHealth;
    public PlayerControls controls;
    public Skill[] skillList;
    // Start is called before the first frame update

    void Start()
    {
        for (int i = 0; i < 2; i++)
        {
            skillList[i].isSkillReady = true;
        }
    }
    private void Awake()
    {
        playerHealth = this.gameObject.GetComponent<Health>();
        controls = this.gameObject.GetComponent<PlayerControls>();
    }

    public void PerformSkill(string direction)
    {
        if (direction == "up" && skillList[0].isSkillReady)
        {
            skillList[0].isSkillReady = false;
            RenderVisual(true, 0);
            skillList[0].PerformSkill(this);
            UIManager.Instance.TriggerCounter(0, skillList[0].coolDown);
        }

        else if (direction == "down" && skillList[1].isSkillReady && !controls.checkIfGrounded())
        {
            skillList[1].isSkillReady = false;
            controls.isSuperSlamActive = true;
            superSlamCollider.transform.position = colliderPoint.transform.position;
            RenderVisual(true, 1);
            skillList[1].PerformSkill(this);
            UIManager.Instance.TriggerCounter(1, skillList[1].coolDown);
        }
    }

    public void RenderVisual(bool isActivate, int index)
    {
        skillPoint.SetActive(isActivate);
        skillPoint.transform.localScale = skillList[index].prefab.transform.localScale;
        skillPoint.GetComponent<SpriteRenderer>().sprite = skillList[index].prefab.GetComponent<SpriteRenderer>().sprite;
        skillPoint.GetComponent<SpriteRenderer>().color = skillList[index].prefab.GetComponent<SpriteRenderer>().color;
    }

    public void DisableSuperSlam()
    {
        skillList[1].GroundHit(this);
    }
}
