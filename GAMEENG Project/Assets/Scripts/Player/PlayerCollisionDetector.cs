using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionDetector : MonoBehaviour
{
    private PlayerControls playerControls;
    private Health playerHealth;

    private void Start()
    {
        playerControls = this.GetComponent<PlayerControls>();
        playerHealth = this.GetComponent<Health>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "GroundEnemy")
        {
            playerHealth.takeDamage(1);
            playerControls.ApplyStompKnockback();
        }

        if (collision.gameObject.GetComponent<GunLoot>())
        {
            var item = collision.gameObject.GetComponent<GunLoot>();
            playerControls.playerWM.EquipGun(item.gunType);
            playerHealth.takeHealth(2);
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "StompTrigger")
        {
            playerControls.ApplyStompKnockback();
            playerControls.ReloadGun();
        }
    }
}