using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//for updating AmmoText when ammo is changed (currentAmmo, maxAmmo)
public class AmmoModified : UnityEvent<int, int> { }

public class WeaponManager : MonoBehaviour
{
    private PlayerControls playerControls;
    private Gun currentGun;
    private Gun[] gunList;

    public Gun startingGun;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public AmmoModified EvtAmmoModified = new AmmoModified();

    void Start()
    {
        playerControls = this.gameObject.GetComponent<PlayerControls>();
        gunList = new Gun[2];
        EquipGun(startingGun);
        currentGun.ResetValues();
        EvtAmmoModified.Invoke(currentGun.GunAmmo.AmmoCount, currentGun.GunAmmo.MagazineSize);
    }

    public void EquipGun(Gun gunType)
    {
        if (!(gunList[0] == gunType) && !(gunList[1] == gunType) && (!gunList[0] || !gunList[1]))
        {
            if (!gunList[0])
                gunList[0] = gunType;

            else if (!gunList[1])
            {
                gunList[1] = gunType;
                UIManager.Instance.RenderGunUI(1, gunType, false);
            }
        }

        else if (!(gunList[0] == gunType) && !(gunList[1] == gunType) && gunList[0] && gunList[1])
        {
            gunList[GetGunIndex(currentGun)] = gunType;
            currentGun = gunType;
            UIManager.Instance.RenderGunUI(GetGunIndex(currentGun), currentGun, true);
        }

        if (!currentGun)
        {
            if (!gunList[0])
                gunList[0] = gunType;

            currentGun = gunType;
            UIManager.Instance.RenderGunUI(0, gunType, true);
        }

        currentGun.ResetValues();

        gunType.setOwner(this);
        EvtAmmoModified.Invoke(currentGun.GunAmmo.AmmoCount, currentGun.GunAmmo.MagazineSize);
    }

    private int GetGunIndex(Gun currentGun)
    {
        for (int i = 0; i < 2; i++)
        {
            if (currentGun == gunList[i])
                return i;
        }

        return 0;
    }

    public void SwitchGun(string direction)
    {
        if (gunList[0] && gunList[1])
        {
            if (direction == "right")
                currentGun = gunList[1];

            else
                currentGun = gunList[0];

            currentGun.ResetValues();

            UIManager.Instance.RenderGunUI(GetGunIndex(currentGun), currentGun, true);
        }
        //For UI
        EvtAmmoModified.Invoke(currentGun.GunAmmo.AmmoCount, currentGun.GunAmmo.MagazineSize);
    }

    public void ReloadGun()
    {
        currentGun.Reload();
        //For UI
        EvtAmmoModified.Invoke(currentGun.GunAmmo.AmmoCount, currentGun.GunAmmo.MagazineSize);
    }

    public void PullWeaponTrigger()
    {
        currentGun.PullTrigger(bulletPrefab, firePoint);
        //For UI
        EvtAmmoModified.Invoke(currentGun.GunAmmo.AmmoCount, currentGun.GunAmmo.MagazineSize);
    }

    public void ReleaseWeaponTrigger()
    {
        currentGun.ReleaseTrigger();
    }

    public void ApplyGunRecoil()
    {
        playerControls.ApplyGunRecoil();
    }
}
