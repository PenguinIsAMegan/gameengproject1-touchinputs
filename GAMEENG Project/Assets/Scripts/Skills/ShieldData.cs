using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Shield Data", menuName = "Skill/Shield")] 
public class ShieldData : Skill
{
    public float Duration = 5;
    //kinda confused if this is the right way to do this...
    protected override void Effect(SkillManager skillManager)
    { 
        skillManager.playerHealth.setShield(true);
        CoroutineHandler.instance.StartCoroutine(SkillTimer(skillManager));
    }

    protected override IEnumerator SkillTimer(SkillManager skillManager)
    {
        yield return new WaitForSeconds(Duration);
        skillManager.playerHealth.setShield(false);
        skillManager.RenderVisual(false, 0);
    }
}
