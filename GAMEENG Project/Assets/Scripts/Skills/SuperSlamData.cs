using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Super Slam Data", menuName = "Skill/Super Slam")]
public class SuperSlamData : Skill
{
    protected override void Effect(SkillManager skillManager)
    {
        skillManager.controls.rb2d.gravityScale = 10;
    }

    public override void GroundHit(SkillManager skillManager)
    {
        skillManager.superSlamCollider.SetActive(true);
        skillManager.RenderVisual(false, 0);
        CoroutineHandler.instance.StartCoroutine(SkillTimer(skillManager));
    }

    protected override IEnumerator SkillTimer(SkillManager skillManager)
    {
        yield return new WaitForSeconds(0.3f);
        skillManager.controls.rb2d.gravityScale = 3;
        skillManager.superSlamCollider.SetActive(false);
    }
}
