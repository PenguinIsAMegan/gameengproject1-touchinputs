using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : ScriptableObject
{
    public GameObject prefab;
    public float coolDown;
    public bool isSkillReady = true;

    protected virtual void Effect(SkillManager skillManager)
    {

    }

    public void PerformSkill(SkillManager skillManager)
    {
        Effect(skillManager);
    }

    protected virtual IEnumerator SkillTimer(SkillManager skillManager)
    {
        yield return new WaitForSeconds(0);
    }

    public virtual void GroundHit(SkillManager skillManager)
    {

    }
}
