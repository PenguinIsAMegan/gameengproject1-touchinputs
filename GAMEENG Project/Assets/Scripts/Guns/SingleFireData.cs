using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Single Fire Gun", menuName = "Gun/Single Fire")]
public class SingleFireData : Gun
{
    protected override void Shoot(GameObject bulletPrefab, Transform firePoint)
    {
        Debug.Log("Shoot Attempt");
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            SingleShot(bulletPrefab, firePoint);
            weaponManager.ApplyGunRecoil();
            GunAmmo.AmmoCount--;
        }
    }

    void SingleShot(GameObject bulletPrefab, Transform firePoint)
    {
        //Shoot a single bullet
        GameObject bullet = ObjectPooler.instance.SpawnFromPool("Bullet", firePoint.position, firePoint.rotation);
        bullet.transform.SetParent(firePoint);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, 0);//reset velocity
        rb.AddForce(bullet.transform.up * -bulletSpeed, ForceMode2D.Impulse);
    }
}
