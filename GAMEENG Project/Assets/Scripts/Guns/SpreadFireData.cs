using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spread Fire Gun", menuName = "Gun/Spread Fire")]
public class SpreadFireData : Gun
{
    public float BulletSpread = 45f;
    protected override void Shoot(GameObject bulletPrefab, Transform firePoint)
    {
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            SpreadShot(bulletPrefab, firePoint);
            weaponManager.ApplyGunRecoil();
            GunAmmo.AmmoCount--;
        }
    }

    void SpreadShot(GameObject bulletPrefab, Transform firePoint)
    {
        Quaternion spreadRotation = Quaternion.Euler(0, 0, -BulletSpread);
        // Spawn 3 bullets instead of 1 at different angles
        for (int i = 0; i < 3; i++)
        {
            GameObject bullet = ObjectPooler.instance.SpawnFromPool("Bullet", firePoint.position, spreadRotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0, 0);//reset velocity
            rb.AddForce(bullet.transform.up * -bulletSpeed, ForceMode2D.Impulse);
            spreadRotation *= Quaternion.Euler(0, 0, BulletSpread);
        }
    }
}