using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Burst Fire Gun", menuName = "Gun/Burst Fire")]
public class BurstFireData : Gun
{
    public float SecondsBetweenBullets = 1f;
    public int BulletsToBeShot = 3;

    protected override void Shoot(GameObject bulletPrefab, Transform firePoint)
    {
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            CoroutineHandler.instance.StartCoroutine(BurstShot(bulletPrefab, firePoint));
            GunAmmo.AmmoCount--;
        }
    }

    IEnumerator BurstShot(GameObject bulletPrefab, Transform firePoint)
    {
        //Spawn 3 bullets, while waiting a bit before firing the other
        for (int i = 0; i < BulletsToBeShot; i++)
        {
            //Shoot a single bullet
            GameObject bullet = ObjectPooler.instance.SpawnFromPool("Bullet", firePoint.position, firePoint.rotation);
            bullet.transform.SetParent(firePoint);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0, 0);//reset velocity
            rb.AddForce(bullet.transform.up * -bulletSpeed, ForceMode2D.Impulse);
            weaponManager.ApplyGunRecoil();
            yield return new WaitForSeconds(SecondsBetweenBullets);
        }
    }
}
