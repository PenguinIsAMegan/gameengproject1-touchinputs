using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Machine Fire Gun", menuName = "Gun/Machine Gun")]
public class MachineGunData : Gun
{
    bool triggerDown;
    public override void PullTrigger(GameObject bulletPrefab, Transform firePoint)
    {
        triggerDown = true;
        Shoot(bulletPrefab, firePoint);
    }

    public override void ReleaseTrigger()
    {
        triggerDown = false;
    }
    protected override void Shoot(GameObject bulletPrefab, Transform firePoint)
    {
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + FireRate;
            CoroutineHandler.instance.StartCoroutine(MachineGunShot(bulletPrefab, firePoint));
        }
    }

    IEnumerator MachineGunShot(GameObject bulletPrefab, Transform firePoint)
    {
        //Loop unitl no ammo or triggerDown is false
        while(triggerDown && GunAmmo.AmmoCount > 0)
        {
            //Shoot a single bullet
            GameObject bullet = ObjectPooler.instance.SpawnFromPool("Bullet", firePoint.position, firePoint.rotation);
            bullet.transform.SetParent(firePoint);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0, 0);//reset velocity
            rb.AddForce(bullet.transform.up * -bulletSpeed, ForceMode2D.Impulse);
            weaponManager.ApplyGunRecoil();
            GunAmmo.AmmoCount--;
            weaponManager.EvtAmmoModified.Invoke(GunAmmo.AmmoCount, GunAmmo.MagazineSize);
            yield return new WaitForSeconds(FireRate);
        }
    }
}
