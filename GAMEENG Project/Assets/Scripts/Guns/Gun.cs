using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun : ScriptableObject
{
    [System.Serializable]
    public class Ammo
    {
        public int MagazineSize = 6;
        public int AmmoCount;
    }

    public Ammo GunAmmo = new Ammo();
    public float bulletSpeed;
    public float FireRate;
    public float nextTimeToFire = 0f;
    public Sprite image;

    protected WeaponManager weaponManager;

    public void setOwner(WeaponManager manager)
    {
        weaponManager = manager;
    }
    
    public void ResetValues()
    {
        Reload();
        nextTimeToFire = 0f;
    }

    public virtual void PullTrigger(GameObject bulletPrefab, Transform firePoint)
    {
        if (GunAmmo.AmmoCount > 0)
        {
            Shoot(bulletPrefab, firePoint);
        }
    }

    public virtual void ReleaseTrigger()
    { 
    
    }

    protected virtual void Shoot(GameObject bulletPrefab, Transform firePoint)
    {
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / FireRate;
            //Shoot goes here
            GunAmmo.AmmoCount--;
        }
    }

    public virtual void Reload()
    {
        if (GunAmmo.AmmoCount < GunAmmo.MagazineSize)
        {
            GunAmmo.AmmoCount = GunAmmo.MagazineSize;
        }
    }
}
