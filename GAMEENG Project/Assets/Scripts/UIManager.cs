using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : Singleton<UIManager>
{
    public new static UIManager Instance;
    public GameObject Player;
    public TextMeshProUGUI HealthText;
    public TextMeshProUGUI AmmoText;
    public TextMeshProUGUI ScoreText;
    public GameObject[] gunUI;
    public GameObject[] cdUI;
    public GameObject GameOverUI;
    public TextMeshProUGUI GameOverScoreText;

    private Health playerHealth;
    private WeaponManager weaponManager;
    private PlayerController playerController;

    void Start()
    {
        if (!Instance)
            Instance = this;

        GameOverUI.SetActive(false);
    }

    void Awake()
    {
        playerHealth = Player.GetComponent<Health>();
        playerHealth.EvtHealthModified.AddListener(HealthTextUpdate);

        weaponManager = Player.GetComponent<WeaponManager>();
        weaponManager.EvtAmmoModified.AddListener(AmmoTextUpdate);

        playerController = Player.GetComponent<PlayerController>();
        playerController.EvtScoreModified.AddListener(ScoreTextUpdate);
    }

    void OnDestroy()
    {
        playerHealth.EvtHealthModified.RemoveListener(HealthTextUpdate);
        weaponManager.EvtAmmoModified.RemoveListener(AmmoTextUpdate);
        playerController.EvtScoreModified.RemoveListener(ScoreTextUpdate);
    }

    void HealthTextUpdate(int value, int maxValue)
    {
        HealthText.text = ("HP: " + value.ToString() + "/" + maxValue.ToString());
    }

    void AmmoTextUpdate(int value, int maxValue)
    {
        AmmoText.text = ("AMMO: " + value.ToString() + "/" + maxValue.ToString());
    }
    void ScoreTextUpdate(int value)
    {
        ScoreText.text = ("Score: " + value.ToString());
    }

    public void RenderGunUI(int index, Gun guntype, bool isCurrentGun)
    {
        gunUI[index].GetComponent<Image>().sprite = guntype.image;
        gunUI[index].GetComponent<Image>().color = Color.white;

        if (isCurrentGun)
        {
            gunUI[index].GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);

            if (index == 0)
                gunUI[1].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);

            else
                gunUI[0].GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
    }

    public void renderSkillVisual(Image image)
    {

    }
    public void TriggerCounter(int index, float cd)
    {
        StartCoroutine(Timer(index, cd));
    }

    public IEnumerator Timer(int index, float cd)
    {
        cdUI[index].SetActive(true);
        float currentTime = cd;

        while (currentTime > 0)
        {
            cdUI[index].GetComponent<TextMeshProUGUI>().text = currentTime.ToString();
            currentTime -= 1;
            yield return new WaitForSeconds(1.0f);
        }

        Player.GetComponent<SkillManager>().skillList[index].isSkillReady = true;
        cdUI[index].SetActive(false);
    }

    public void ShowGameOverScreen()
    {
        GameOverUI.SetActive(true);
        GameOverScoreText.text = ScoreText.text;
    }

    public void RestartWorld()
    {
        SceneManager.LoadScene("World");
    }
}
