using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthModified : UnityEvent<int, int> { }

public class Health : MonoBehaviour
{
    private Unit owner;
    public int maxHP;
    private bool hasShield;

    public int currentHP;

    public HealthModified EvtHealthModified = new HealthModified();

    public void init(int hp, Unit mOwner)
    {
        this.maxHP = hp;
        this.currentHP = maxHP;
        this.owner = mOwner;
        this.hasShield = false;
        EvtHealthModified.Invoke(currentHP, maxHP);
    }

    public void takeDamage(int damage)
    {
        if(!hasShield)
            this.currentHP -= damage;

        if (this.currentHP <= 0)
            owner.Die();
        EvtHealthModified.Invoke(currentHP, maxHP);
    }

    public void takeHealth(int healing)
    {
        if (currentHP < maxHP)
        {
            currentHP += healing;
            currentHP = Mathf.Clamp(currentHP, 0, maxHP);
        }
        EvtHealthModified.Invoke(currentHP, maxHP);
    }

    public void setShield(bool boolSetter)
    {
        hasShield = boolSetter;
    }
}
