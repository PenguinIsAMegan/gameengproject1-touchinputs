using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Unit : MonoBehaviour
{
    [SerializeField]
    protected Health health;

    public UnityAction Death; // Kinda unsure if UnityAction == Action (for the events thing)

    public virtual void Start()
    {

    }

    public void init(int hp, Unit owner)
    {
        if (this.GetComponent<Health>())
        {
            this.health = this.GetComponent<Health>();
            this.health.init(hp, owner);
        }
    }

    public virtual void GetHit(int damage)
    {
        if (health != null)
            health.takeDamage(damage);
        else
            Debug.LogError(this.gameObject.name + " does not have the proper Health Component so it can't take damage!");
    }

    public virtual void Die()
    {

    }
}
